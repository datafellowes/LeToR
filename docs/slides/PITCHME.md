--- 
## Sternshus & UW  
---
## Query Graph Expansion  
<a title="By dfaulder (Ruby-throated Hummingbirds) [CC BY 2.0 (http://creativecommons.org/licenses/by/2.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ARuby-throated_Hummingbirds_(15268384737).jpg"><img width="512" alt="Ruby-throated Hummingbirds (15268384737)" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Ruby-throated_Hummingbirds_%2815268384737%29.jpg/512px-Ruby-throated_Hummingbirds_%2815268384737%29.jpg"/></a>

Note:
Query Graph Expansion
* Expansion
    * mismatch between query & results 
    * synonyms: (auto, automobile, car)
    * entity: Taj Mahal, New York, 
    * sets: British Royal Family, 
    * query graphs: set of symptoms matching diseases ranked by likelihood
* Knowledge Graph
    * RDF (DBPedia)
    * Entities
    * Relationships between Entities
* Query (2012 Google Hummingbird Algorithm)
    * keywords on knowledge graph 
* Entity (2014 -2016)
    * entity on knowledge graph 
* Query Graph
    * entities
    * relationships between entities
    * knowledge graph alignment
--- 
## LeToR  
### (Learning To Rank)  


    
Learning To Rank
Explorations in Learning To Rank for entity expansion.  First steps, reproducing current state of the art.

--- 
## Reproducing Entity Expansion

